from __future__ import print_function
import threading
import datetime

import grpc

import interface_pb2
import interface_pb2_grpc

technicianName = "TECHNICIAN1"

def listRequestedExaminations(stub):
  query = interface_pb2.Query()
  query.parameters["dateCompleted"].val = ""
  query.parameters["dateCompleted"].matchType = interface_pb2.EXACT
  i = 1
  for result in stub.SearchExaminations(query):
    print("\nReceived result (no." + str(i) + "):\n" + str(result) + "\n")
    i = i + 1
  print("End of list")  

def printHelp():
  print("----------Usage----------")
  print("/list\tList current requests")
  print("/put\tPut result")
  print("/exit\tExit the program")
  print("-------------------------")


def run():
  channel = grpc.insecure_channel('localhost:50051')
  stub = interface_pb2_grpc.ExaminationHandlerStub(channel)
  technicianName = input("Enter your name: ")
  command = "help"
  while command != "/exit":
    if command == "help":
      printHelp()
    elif command == "/list":
      threading._start_new_thread(listRequestedExaminations, (stub,))
    elif command == "/put":
      id = int(input("Enter id of chosen examination:\n"))
      tests = {}
      key = ""
      while True:
        key = input("Enter key of chosen test. Enter 'done' to finish: ")
        if key == "done":
          break
        val = input("Enter value of test " + key + ":\n")
        tests[key] = val  
      result = stub.PutResults.future(interface_pb2.Examination(id=id, patient=None, doctor=None,
                                      technician=interface_pb2.Technician(name=technicianName),
                                      dateRequested=None, dateCompleted=None, tests=tests))  
      result.add_done_callback(lambda res: print("Received ack for put results: " + str(res.result())))

    command = input("Enter command: \n")

  print("Exiting program")

if __name__ == '__main__':
  run()
