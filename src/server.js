'use strict';

var PROTO_PATH = __dirname + '/../interface.proto';

var grpc = require('grpc');
var proto = grpc.load(PROTO_PATH).agh;

function getRandomId() {
  return Math.floor(Math.random()*1000000000000000);
}

function queryMatches(left, right, matchType) {
  function cmpStr(a, b) {
    for(var i = 0; i<a.length && i<b.length;i++) {
      if(a.charCodeAt(i) < b.charCodeAt(i)) {
        return -1;
      }
      if(a.charCodeAt(i) > b.charCodeAt(i)) {
        return 1;
      }
    }
    if(a.length < b.length) {
      return -1;
    }
    if(a.length > b.length) {
      return 1;
    }
    return 0;
  }
  if(matchType === "EXACT") {
    return left !== undefined && (left.name === undefined ? left === right : left.name === right);
  }
  if(matchType === "LESS") {
    return left !== undefined && (left.name === undefined ? cmpStr(left, right) < 0 : cmpStr(left.name, right) < 0);
  }
  if(matchType === "GREATER") {
    return left !== undefined && (left.name === undefined ? cmpStr(left, right) > 0 : cmpStr(left.name, right) > 0);
  }
  console.log("queryMatches: invalid match type")
}

function compareKeys(a, b) {
  var aKeys = Object.keys(a).sort();
  var bKeys = Object.keys(b).sort();
  return JSON.stringify(aKeys) === JSON.stringify(bKeys);
}

var completedExaminations = [
  // prefilled with test values
  {
    id: 1111,
    patient: {name: "testpatient"},
    doctor: {name: "testdoctor"},
    technician: {name: "testtechnician"},
    dateRequested: "testdate",
    dateCompleted: "testdate",
    tests: {
      "blood": "ok",
      "cancer": "nope"
    }
  }
];
var requestedExaminations = [];

function GetResults(call) {
  console.log("Get results requested!")  
  for(var i = 0; i < completedExaminations.length; i++) {
    if(completedExaminations[i].patient.name === call.request.name) {
      call.write(completedExaminations[i]);
    }
  }
  call.end();
}

function PutResults(call, callback) {
  console.log("Put results requested!")
  
  var request = call.request;
  var response = {
    isOk: false,
    errorMsg: ""
  }
  var index = requestedExaminations.findIndex(examination => {
    return examination.id == request.id; 
  });
  if (index >= 0) {
    var foundExamination = requestedExaminations[index];
    if(!request.technician) {
      response.msg += 'Error: Expected techician name in the request\n';
    }
    // if(!request.dateCompleted) {
    //   response.msg += 'Error: Expected date of completion in the request\n';
    // }
    if(!compareKeys(request.tests, foundExamination.tests)) {
      response.errorMsg += 'Error: Expected the same tests with results in the request\n';
    }
    if(!response.errorMsg) {
      requestedExaminations.splice(index, 1); // remove from requestedExaminations list
      foundExamination.technician = request.technician;
      foundExamination.dateCompleted = new Date().toDateString();
      foundExamination.tests = request.tests;
      completedExaminations.push(foundExamination);
      response.isOk = true;
    }
  }
  else {
    response.msg += 'Examination with requested id not found\n';
  }
  callback(null, response);
}

function RequestExamination(call, callback) {
  console.log("Examination requested!");
  console.log(call.request);
  var request = call.request;
  var response = {
    isOk: false,
    errorMsg: ""
  }
  // if(!request.id) {
  //   response.errorMsg += 'Error: Expected id in the request\n';
  // }
  if(!request.doctor) {
    response.errorMsg += 'Error: Expected doctor name in the request\n';
  }
  if(!request.patient) {
    response.errorMsg += 'Error: Expected patient name in the request\n';
  }
  // if(!response.dateRequested) {
  //   response.errorMsg += 'Error: Expected date of request in the request\n';
  // }
  if(!response.errorMsg) {
    response.isOk = true;
    request.id = getRandomId();
    request.dateRequested = new Date().toDateString();
    requestedExaminations.push(request);
  }
  callback(null, response);
}

function SearchExaminations(call) {
  console.log("Searching examinations requested");
  console.log(call.request);
  var keys = Object.keys(call.request.parameters);
  var iteratedCollection = requestedExaminations.concat(completedExaminations);
  keys.forEach(function(key) {
    var results = [];
    iteratedCollection.forEach(function(elem) {
      if(queryMatches(elem[key], call.request.parameters[key].val, call.request.parameters[key].matchType)) {
        results.push(elem);
      } else if (elem.tests && queryMatches(elem.tests[key], call.request.parameters[key].val, call.request.parameters[key].matchType)) {
        results.push(elem);
      }
    });
    iteratedCollection = results;
  });
  iteratedCollection.forEach(function(res){
    call.write(res);
  });
  call.end()
}

function main() {
  var server = new grpc.Server();
  server.addProtoService(proto.ExaminationHandler.service, {
    GetResults: GetResults,
    PutResults: PutResults,
    RequestExamination: RequestExamination,
    SearchExaminations: SearchExaminations
  });
  server.bind('0.0.0.0:50051', grpc.ServerCredentials.createInsecure());
  server.start();
}

main();
