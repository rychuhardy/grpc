from __future__ import print_function
import threading
import grpc

import interface_pb2
import interface_pb2_grpc

def getPatientResult(patientName, stub):
  patient = interface_pb2.Patient(name=patientName)
  i = 1
  for response in stub.GetResults(patient):
    print("Result received(no." + str(i) + "):\n" + str(response))
    i = i+1
  print("End of results")  

def printHelp():
  print("----------Usage----------")
  print("/get\tGet patients results")
  print("/exit\tExit the program")
  print("-------------------------")


def run():
  channel = grpc.insecure_channel('localhost:50051')
  stub = interface_pb2_grpc.ExaminationHandlerStub(channel)

  command = "help"
  while command != "/exit":
    if command == "help":
      printHelp()
    elif command == "/get":
      patientName = input("Enter patient name: ")
      threading._start_new_thread(getPatientResult, (patientName, stub))
    command = input("Enter command: \n")

  print("Exiting program")

if __name__ == '__main__':
  run()
