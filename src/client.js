var PROTO_PATH = __dirname + '/../interface.proto';

var grpc = require('grpc');
var hello_proto = grpc.load(PROTO_PATH).agh;

function main() {
  var client = new hello_proto.ExaminationHandler('localhost:50051',
                                       grpc.credentials.createInsecure());
  var user;
  if (process.argv.length >= 3) {
    user = process.argv[2];
  } else {
    user = 'world';
  }
  var call = client.getResults({name: "testpatient"});
  call.on('data', function(feature) {
      console.log('Found feature called "' + feature.doctor + '" at ');
  });
  call.on('end', function() {
    console.log("finieshe");
  });
  call.on('status', function(status) {
    // process status
  });
}

main();