from __future__ import print_function
import threading
import grpc

import interface_pb2
import interface_pb2_grpc

def getQueryResult(query, stub):
  i = 1
  for response in stub.SearchExaminations(query):
    print("\nResult received (no." + str(i) + "):\n" + str(response))
    i = i + 1
  print("End of query")  

def printHelp():
  print("----------Usage----------")
  print("/find\tFind results")
  print("/request\tRequest examination")
  print("/exit\tExit the program")
  print("-------------------------")


def getMatchTypeFromStr(str):
  if str == "less":
    return interface_pb2.LESS
  if str == "equal":
    return interface_pb2.EXACT
  if str == "greater":
    return interface_pb2.GREATER
  raise ValueError("Invalid value of MatchType " + str)


def run():
  channel = grpc.insecure_channel('localhost:50051')
  stub = interface_pb2_grpc.ExaminationHandlerStub(channel)
  doctorName = input("Enter your name: ")
  command = "help"
  while command != "/exit":
    if command == "help":
      printHelp()
    elif command == "/find":
      query = interface_pb2.Query()
      patient = input("Enter patient name (or leave empty): ")
      if patient != "":
        while True:
          type = input("Enter one of: less, equal or greater to select match type ")
          try:
            matchType = getMatchTypeFromStr(type)
            query.parameters['patient'].val = patient
            query.parameters['patient'].matchType = matchType
            break
          except ValueError as ex:
            print("Invalid value for match type: " + type)
      test = input("Enter test name (or leave empty): ")
      if test != "":
        value = input("Enter test result: ")
        while True:
          type = input("Enter one of: less, equal or greater to select match type ")
          try:
            matchType = getMatchTypeFromStr(type)
            query.parameters[test].val = value
            query.parameters[test].matchType = matchType
            break
          except ValueError as ex:
            print("Invalid value for match type " + type)
      # print(str(query))    
      threading._start_new_thread(getQueryResult, (query, stub))     
      
    elif command == "/request":
      examination = interface_pb2.Examination(id=0,
                                              patient=interface_pb2.Patient(name=""),
                                              doctor=interface_pb2.Doctor(name=doctorName),
                                              technician=interface_pb2.Technician(name=""),
                                              dateRequested="", dateCompleted="", tests = {})      
      name = input("Enter patient name: ")
      examination.patient.name = name
      testname = ""
      while True:
        testname = input("Enter requested test(enter done to finish): ")
        if testname == "done":
          break
        if testname != "":  
          examination.tests[testname] = "todo"
      # print(str(examination))
      result = stub.RequestExamination.future(examination)  
      result.add_done_callback(lambda res: print("Received ack for request: " + str(res.result())))   
      

    command = input("Enter command: \n")

  print("Exiting program")

if __name__ == '__main__':
  run()
